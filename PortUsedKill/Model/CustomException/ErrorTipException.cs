﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortUsedKill.Model.CustomException
{
    [Serializable]
    public class ErrorTipException : ApplicationException
    {
        private readonly ExceptionResult _exceptionResult;
        public ErrorTipException() { }
        public ErrorTipException(string message) : base(message)
        {
            _exceptionResult = new ExceptionResult { Message = message };
        }
        public ExceptionResult GetExceptionResult()
        {
            return _exceptionResult;
        }
    }
    public class ExceptionResult
    {
        public string Message { get; set; }
    }
}
