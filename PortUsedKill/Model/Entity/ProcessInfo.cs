﻿using PortUsedKill.util;
using PortUsedKill.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortUsedKill.Model.Entity
{
    public class ProcessInfo
    {
        public Icon ProcessIcon { get; set; }
        /// <summary>
        /// 进程名称
        /// </summary>
        public string ProcessName { get; set; }
        /// <summary>
        /// 协议类型
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 进程PID
        /// </summary>
        public int ProcessIdentification { get; set; }
        /// <summary>
        /// 端口
        /// </summary>
        public string Port { get; set; }
        /// <summary>
        /// 本地地址
        /// </summary>
        public string LocalAddress { get; set; }
        /// <summary>
        /// 远程地址
        /// </summary>
        public string RemoteAddress { get; set; }
        /// <summary>
        /// 远程IP
        /// </summary>
        private string RemoteIp { get; set; }
        /// <summary>
        /// 远程IP位置
        /// </summary>
        public string RemoteIpLocation { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public TCP_CONNECTION_STATE State { get; set; }

        public static ProcessInfoBulider Bulider()
        {
            return new ProcessInfoBulider();
        }

        public class ProcessInfoBulider
        {
            private Icon processIcon;
            /// <summary>
            /// 进程名称
            /// </summary>
            private string processName;
            /// <summary>
            /// 协议类型
            /// </summary>
            private string type;
            /// <summary>
            /// 进程PID
            /// </summary>
            private int processIdentification;
            /// <summary>
            /// 端口
            /// </summary>
            private string port;
            /// <summary>
            /// 本地地址
            /// </summary>
            private string localAddress;
            /// <summary>
            /// 远程地址
            /// </summary>
            private string remoteAddress;
            /// <summary>
            /// 远程IP
            /// </summary>
            private string remoteIp;
            /// <summary>
            /// 状态
            /// </summary>
            private TCP_CONNECTION_STATE state;

            public ProcessInfoBulider ProcessIcon(Icon processIcon)
            {
                this.processIcon = processIcon;
                return this;
            }

            public ProcessInfoBulider ProcessName(string processName)
            {
                this.processName = processName;
                return this;
            }

            public ProcessInfoBulider Type(string type)
            {
                this.type = type;
                return this;
            }

            public ProcessInfoBulider ProcessIdentification(int processIdentification)
            {
                this.processIdentification = processIdentification;
                return this;
            }

            public ProcessInfoBulider Port(string port)
            {
                this.port = port;
                return this;
            }

            public ProcessInfoBulider LocalAddress(string localAddress)
            {
                this.localAddress = localAddress;
                return this;
            }

            public ProcessInfoBulider RemoteAddress(string remoteAddress)
            {
                this.remoteAddress = remoteAddress;
                return this;
            }

            public ProcessInfoBulider RemoteIp(string remoteIp)
            {
                this.remoteIp = remoteIp;
                return this;
            }

            public ProcessInfoBulider State(TCP_CONNECTION_STATE state)
            {
                this.state = state;
                return this;
            }

            public ProcessInfo Bulid()
            {
                return new ProcessInfo(processIcon, processName, type, processIdentification, port, localAddress, remoteAddress, remoteIp, state);
            }
        }

        public ProcessInfo() { }

        public ProcessInfo(Icon processIcon, string processName, string type, int processIdentification, string port, string localAddress, string remoteAddress, string remoteIp, TCP_CONNECTION_STATE state)
        {
            ProcessIcon = processIcon;
            ProcessName = processName;
            Type = type;
            ProcessIdentification = processIdentification;
            Port = port;
            LocalAddress = localAddress;
            RemoteAddress = remoteAddress;
            RemoteIp = remoteIp;
            State = state;
        }

        class AddressInfo
        {
            public string pro { get; set; }

            public string city { get; set; }
        }

        public void GetRemoteIpLocation()
        {
            if (RemoteIp == null)
            {
                RemoteIpLocation = "xx xx";
                return;
            }
            if (IpUtils.IpIsInner(RemoteIp))
            {
                RemoteIpLocation = "内网IP";
                return;
            }
            string url = $"https://whois.pconline.com.cn/ipJson.jsp?ip={this.RemoteIp}&json=true";
            AddressInfo address = HttpHelper.GetHttpResponse<AddressInfo>(url, 6000);
            if (address == null)
            {
                this.RemoteIpLocation = "未知";
            }
            this.RemoteIpLocation = $"{address.pro} {address.city}";
        }
    }
}
