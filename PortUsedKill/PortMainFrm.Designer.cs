﻿
namespace PortUsedKill
{
    partial class PortMainFrm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PortMainFrm));
            this.tabMenu = new Sunny.UI.UITabControl();
            this.tabPagePortKill = new System.Windows.Forms.TabPage();
            this.uiTxtPort = new Sunny.UI.UITextBox();
            this.uiDgvProcessList = new Sunny.UI.UIDataGridView();
            this.ProcessIcon = new System.Windows.Forms.DataGridViewImageColumn();
            this.ProcessName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessIdentification = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Port = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LocalAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RemoteAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RemoteIpLocation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.State = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uiRightMenuProcess = new Sunny.UI.UIContextMenuStrip();
            this.tsmKill = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tmsOpenFilePath = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsCopyFilePath = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.tmsFileAttributes = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.TsmIssues = new System.Windows.Forms.ToolStripMenuItem();
            this.uiProcessBar = new Sunny.UI.UIProcessBar();
            this.uiBtnScan = new Sunny.UI.UIButton();
            this.tabPageCmdDebug = new System.Windows.Forms.TabPage();
            this.RichTextCmdView = new Sunny.UI.UIRichTextBox();
            this.BtnExecute = new Sunny.UI.UIButton();
            this.TxtCmd = new Sunny.UI.UITextBox();
            this.CboCmd = new Sunny.UI.UIComboBox();
            this.uiStyleManager1 = new Sunny.UI.UIStyleManager(this.components);
            this.uiLalPrompt = new Sunny.UI.UILabel();
            this.tabMenu.SuspendLayout();
            this.tabPagePortKill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiDgvProcessList)).BeginInit();
            this.uiRightMenuProcess.SuspendLayout();
            this.tabPageCmdDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMenu
            // 
            this.tabMenu.Controls.Add(this.tabPagePortKill);
            this.tabMenu.Controls.Add(this.tabPageCmdDebug);
            this.tabMenu.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabMenu.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabMenu.ItemSize = new System.Drawing.Size(150, 40);
            this.tabMenu.Location = new System.Drawing.Point(3, 38);
            this.tabMenu.MainPage = "";
            this.tabMenu.MenuStyle = Sunny.UI.UIMenuStyle.Custom;
            this.tabMenu.Name = "tabMenu";
            this.tabMenu.SelectedIndex = 0;
            this.tabMenu.Size = new System.Drawing.Size(877, 461);
            this.tabMenu.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabMenu.Style = Sunny.UI.UIStyle.Custom;
            this.tabMenu.TabBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.tabMenu.TabIndex = 0;
            this.tabMenu.TabSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.tabMenu.TabUnSelectedForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.tabMenu.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            // 
            // tabPagePortKill
            // 
            this.tabPagePortKill.BackColor = System.Drawing.Color.White;
            this.tabPagePortKill.Controls.Add(this.uiTxtPort);
            this.tabPagePortKill.Controls.Add(this.uiDgvProcessList);
            this.tabPagePortKill.Controls.Add(this.uiProcessBar);
            this.tabPagePortKill.Controls.Add(this.uiBtnScan);
            this.tabPagePortKill.Location = new System.Drawing.Point(0, 40);
            this.tabPagePortKill.Name = "tabPagePortKill";
            this.tabPagePortKill.Size = new System.Drawing.Size(877, 421);
            this.tabPagePortKill.TabIndex = 0;
            this.tabPagePortKill.Text = "端口占用查杀";
            // 
            // uiTxtPort
            // 
            this.uiTxtPort.ButtonSymbol = 61442;
            this.uiTxtPort.ButtonSymbolOffset = new System.Drawing.Point(1, 1);
            this.uiTxtPort.ButtonWidth = 35;
            this.uiTxtPort.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.uiTxtPort.FocusedSelectAll = true;
            this.uiTxtPort.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiTxtPort.Location = new System.Drawing.Point(15, 17);
            this.uiTxtPort.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTxtPort.Maximum = 2147483647D;
            this.uiTxtPort.Minimum = -2147483648D;
            this.uiTxtPort.MinimumSize = new System.Drawing.Size(1, 16);
            this.uiTxtPort.Name = "uiTxtPort";
            this.uiTxtPort.ShowButton = true;
            this.uiTxtPort.Size = new System.Drawing.Size(258, 35);
            this.uiTxtPort.Style = Sunny.UI.UIStyle.Custom;
            this.uiTxtPort.TabIndex = 7;
            this.uiTxtPort.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiTxtPort.Watermark = "请输入端口号";
            this.uiTxtPort.WordWarp = false;
            this.uiTxtPort.ButtonClick += new System.EventHandler(this.UiTxtPort_ButtonClick);
            this.uiTxtPort.KeyDown += new System.Windows.Forms.KeyEventHandler(this.UiTxtPort_KeyDown);
            // 
            // uiDgvProcessList
            // 
            this.uiDgvProcessList.AllowUserToAddRows = false;
            this.uiDgvProcessList.AllowUserToDeleteRows = false;
            this.uiDgvProcessList.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.uiDgvProcessList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.uiDgvProcessList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.uiDgvProcessList.BackgroundColor = System.Drawing.Color.White;
            this.uiDgvProcessList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.uiDgvProcessList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.uiDgvProcessList.ColumnHeadersHeight = 32;
            this.uiDgvProcessList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.uiDgvProcessList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProcessIcon,
            this.ProcessName,
            this.Type,
            this.ProcessIdentification,
            this.Port,
            this.LocalAddress,
            this.RemoteAddress,
            this.RemoteIpLocation,
            this.State});
            this.uiDgvProcessList.ContextMenuStrip = this.uiRightMenuProcess;
            this.uiDgvProcessList.EnableHeadersVisualStyles = false;
            this.uiDgvProcessList.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiDgvProcessList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.uiDgvProcessList.Location = new System.Drawing.Point(13, 59);
            this.uiDgvProcessList.MultiSelect = false;
            this.uiDgvProcessList.Name = "uiDgvProcessList";
            this.uiDgvProcessList.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.uiDgvProcessList.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.uiDgvProcessList.RowHeadersVisible = false;
            this.uiDgvProcessList.RowHeadersWidth = 51;
            this.uiDgvProcessList.RowHeight = 27;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            this.uiDgvProcessList.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.uiDgvProcessList.RowTemplate.Height = 27;
            this.uiDgvProcessList.SelectedIndex = -1;
            this.uiDgvProcessList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.uiDgvProcessList.ShowGridLine = true;
            this.uiDgvProcessList.Size = new System.Drawing.Size(855, 347);
            this.uiDgvProcessList.Style = Sunny.UI.UIStyle.Custom;
            this.uiDgvProcessList.TabIndex = 6;
            this.uiDgvProcessList.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.DgvProcessList_CellFormatting);
            // 
            // ProcessIcon
            // 
            this.ProcessIcon.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ProcessIcon.DataPropertyName = "ProcessIcon";
            this.ProcessIcon.HeaderText = "";
            this.ProcessIcon.MinimumWidth = 6;
            this.ProcessIcon.Name = "ProcessIcon";
            this.ProcessIcon.ReadOnly = true;
            this.ProcessIcon.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ProcessIcon.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ProcessIcon.Width = 50;
            // 
            // ProcessName
            // 
            this.ProcessName.DataPropertyName = "ProcessName";
            this.ProcessName.FillWeight = 69.54016F;
            this.ProcessName.HeaderText = "进程名称";
            this.ProcessName.MinimumWidth = 6;
            this.ProcessName.Name = "ProcessName";
            this.ProcessName.ReadOnly = true;
            // 
            // Type
            // 
            this.Type.DataPropertyName = "Type";
            this.Type.FillWeight = 69.54016F;
            this.Type.HeaderText = "协议";
            this.Type.MinimumWidth = 6;
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            // 
            // ProcessIdentification
            // 
            this.ProcessIdentification.DataPropertyName = "ProcessIdentification";
            this.ProcessIdentification.FillWeight = 69.54016F;
            this.ProcessIdentification.HeaderText = "进程PID";
            this.ProcessIdentification.MinimumWidth = 6;
            this.ProcessIdentification.Name = "ProcessIdentification";
            this.ProcessIdentification.ReadOnly = true;
            // 
            // Port
            // 
            this.Port.DataPropertyName = "Port";
            this.Port.FillWeight = 69.54016F;
            this.Port.HeaderText = "端口";
            this.Port.MinimumWidth = 6;
            this.Port.Name = "Port";
            this.Port.ReadOnly = true;
            // 
            // LocalAddress
            // 
            this.LocalAddress.DataPropertyName = "LocalAddress";
            this.LocalAddress.FillWeight = 69.54016F;
            this.LocalAddress.HeaderText = "本地地址";
            this.LocalAddress.MinimumWidth = 6;
            this.LocalAddress.Name = "LocalAddress";
            this.LocalAddress.ReadOnly = true;
            // 
            // RemoteAddress
            // 
            this.RemoteAddress.DataPropertyName = "RemoteAddress";
            this.RemoteAddress.FillWeight = 69.54016F;
            this.RemoteAddress.HeaderText = "远程地址";
            this.RemoteAddress.MinimumWidth = 6;
            this.RemoteAddress.Name = "RemoteAddress";
            this.RemoteAddress.ReadOnly = true;
            // 
            // RemoteIpLocation
            // 
            this.RemoteIpLocation.DataPropertyName = "RemoteIpLocation";
            this.RemoteIpLocation.FillWeight = 69.54016F;
            this.RemoteIpLocation.HeaderText = "远程IP位置";
            this.RemoteIpLocation.MinimumWidth = 6;
            this.RemoteIpLocation.Name = "RemoteIpLocation";
            this.RemoteIpLocation.ReadOnly = true;
            this.RemoteIpLocation.Visible = false;
            // 
            // State
            // 
            this.State.DataPropertyName = "State";
            this.State.FillWeight = 69.54016F;
            this.State.HeaderText = "状态";
            this.State.MinimumWidth = 6;
            this.State.Name = "State";
            this.State.ReadOnly = true;
            // 
            // uiRightMenuProcess
            // 
            this.uiRightMenuProcess.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiRightMenuProcess.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.uiRightMenuProcess.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmKill,
            this.tsmRefresh,
            this.toolStripMenuItem1,
            this.tmsOpenFilePath,
            this.tmsCopyFilePath,
            this.toolStripMenuItem2,
            this.tmsFileAttributes,
            this.toolStripMenuItem3,
            this.TsmIssues});
            this.uiRightMenuProcess.Name = "uiRightMenuProcess";
            this.uiRightMenuProcess.Size = new System.Drawing.Size(205, 214);
            // 
            // tsmKill
            // 
            this.tsmKill.Name = "tsmKill";
            this.tsmKill.Size = new System.Drawing.Size(204, 32);
            this.tsmKill.Text = "终止此进程";
            this.tsmKill.Click += new System.EventHandler(this.ToolStripMenuKill_Click);
            // 
            // tsmRefresh
            // 
            this.tsmRefresh.Name = "tsmRefresh";
            this.tsmRefresh.Size = new System.Drawing.Size(204, 32);
            this.tsmRefresh.Text = "刷新进程列表";
            this.tsmRefresh.Click += new System.EventHandler(this.ToolStripMenuRefresh_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(201, 6);
            // 
            // tmsOpenFilePath
            // 
            this.tmsOpenFilePath.Name = "tmsOpenFilePath";
            this.tmsOpenFilePath.Size = new System.Drawing.Size(204, 32);
            this.tmsOpenFilePath.Text = "打开文件路径";
            this.tmsOpenFilePath.Click += new System.EventHandler(this.ToolStripMenuOpenFilePath_Click);
            // 
            // tmsCopyFilePath
            // 
            this.tmsCopyFilePath.Name = "tmsCopyFilePath";
            this.tmsCopyFilePath.Size = new System.Drawing.Size(204, 32);
            this.tmsCopyFilePath.Text = "复制完整路径";
            this.tmsCopyFilePath.Click += new System.EventHandler(this.ToolStripMenuCopyFilePath_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(201, 6);
            // 
            // tmsFileAttributes
            // 
            this.tmsFileAttributes.Name = "tmsFileAttributes";
            this.tmsFileAttributes.Size = new System.Drawing.Size(204, 32);
            this.tmsFileAttributes.Text = "属性";
            this.tmsFileAttributes.Click += new System.EventHandler(this.ToolStripMenuFileAttributes_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(201, 6);
            // 
            // TsmIssues
            // 
            this.TsmIssues.Name = "TsmIssues";
            this.TsmIssues.Size = new System.Drawing.Size(204, 32);
            this.TsmIssues.Text = "联系Issues";
            this.TsmIssues.Click += new System.EventHandler(this.TsmIssues_Click);
            // 
            // uiProcessBar
            // 
            this.uiProcessBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiProcessBar.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiProcessBar.Location = new System.Drawing.Point(0, 0);
            this.uiProcessBar.MinimumSize = new System.Drawing.Size(70, 3);
            this.uiProcessBar.Name = "uiProcessBar";
            this.uiProcessBar.ShowPercent = false;
            this.uiProcessBar.ShowValue = false;
            this.uiProcessBar.Size = new System.Drawing.Size(877, 3);
            this.uiProcessBar.Style = Sunny.UI.UIStyle.Custom;
            this.uiProcessBar.TabIndex = 4;
            this.uiProcessBar.Text = "uiProcessBar1";
            this.uiProcessBar.Visible = false;
            // 
            // uiBtnScan
            // 
            this.uiBtnScan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiBtnScan.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiBtnScan.Location = new System.Drawing.Point(731, 16);
            this.uiBtnScan.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiBtnScan.Name = "uiBtnScan";
            this.uiBtnScan.Size = new System.Drawing.Size(137, 35);
            this.uiBtnScan.Style = Sunny.UI.UIStyle.Custom;
            this.uiBtnScan.TabIndex = 0;
            this.uiBtnScan.Text = "扫描全部";
            this.uiBtnScan.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiBtnScan.Click += new System.EventHandler(this.BtnScan_Click);
            // 
            // tabPageCmdDebug
            // 
            this.tabPageCmdDebug.BackColor = System.Drawing.Color.White;
            this.tabPageCmdDebug.Controls.Add(this.RichTextCmdView);
            this.tabPageCmdDebug.Controls.Add(this.BtnExecute);
            this.tabPageCmdDebug.Controls.Add(this.TxtCmd);
            this.tabPageCmdDebug.Controls.Add(this.CboCmd);
            this.tabPageCmdDebug.Location = new System.Drawing.Point(0, 40);
            this.tabPageCmdDebug.Name = "tabPageCmdDebug";
            this.tabPageCmdDebug.Size = new System.Drawing.Size(877, 421);
            this.tabPageCmdDebug.TabIndex = 1;
            this.tabPageCmdDebug.Text = "cmd命令调试";
            // 
            // RichTextCmdView
            // 
            this.RichTextCmdView.AutoWordSelection = true;
            this.RichTextCmdView.BackColor = System.Drawing.Color.CornflowerBlue;
            this.RichTextCmdView.FillColor = System.Drawing.Color.CornflowerBlue;
            this.RichTextCmdView.Font = new System.Drawing.Font("新宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.RichTextCmdView.ForeColor = System.Drawing.Color.White;
            this.RichTextCmdView.Location = new System.Drawing.Point(18, 110);
            this.RichTextCmdView.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.RichTextCmdView.MinimumSize = new System.Drawing.Size(1, 1);
            this.RichTextCmdView.Name = "RichTextCmdView";
            this.RichTextCmdView.Padding = new System.Windows.Forms.Padding(2);
            this.RichTextCmdView.ReadOnly = true;
            this.RichTextCmdView.Size = new System.Drawing.Size(843, 289);
            this.RichTextCmdView.Style = Sunny.UI.UIStyle.Custom;
            this.RichTextCmdView.TabIndex = 5;
            this.RichTextCmdView.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RichTextCmdView.WordWrap = true;
            // 
            // BtnExecute
            // 
            this.BtnExecute.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnExecute.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnExecute.Location = new System.Drawing.Point(672, 19);
            this.BtnExecute.MinimumSize = new System.Drawing.Size(1, 1);
            this.BtnExecute.Name = "BtnExecute";
            this.BtnExecute.Size = new System.Drawing.Size(146, 35);
            this.BtnExecute.Style = Sunny.UI.UIStyle.Custom;
            this.BtnExecute.TabIndex = 3;
            this.BtnExecute.Text = "执行";
            this.BtnExecute.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnExecute.Click += new System.EventHandler(this.BtnExecute_Click);
            // 
            // TxtCmd
            // 
            this.TxtCmd.ButtonSymbol = 61761;
            this.TxtCmd.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtCmd.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TxtCmd.Location = new System.Drawing.Point(18, 65);
            this.TxtCmd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TxtCmd.Maximum = 2147483647D;
            this.TxtCmd.Minimum = -2147483648D;
            this.TxtCmd.MinimumSize = new System.Drawing.Size(1, 16);
            this.TxtCmd.Name = "TxtCmd";
            this.TxtCmd.Size = new System.Drawing.Size(589, 35);
            this.TxtCmd.Style = Sunny.UI.UIStyle.Custom;
            this.TxtCmd.TabIndex = 2;
            this.TxtCmd.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.TxtCmd.Watermark = "请输入调试命令";
            // 
            // CboCmd
            // 
            this.CboCmd.DataSource = null;
            this.CboCmd.DropDownStyle = Sunny.UI.UIDropDownStyle.DropDownList;
            this.CboCmd.FillColor = System.Drawing.Color.White;
            this.CboCmd.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.CboCmd.Items.AddRange(new object[] {
            "查看端口占用",
            "查找与端口相关占用(引号内输入端口号)",
            "通过进程PID获取占用程序名(引号内输入进程PID)",
            "结束进程(引号内输入进程PID)"});
            this.CboCmd.Location = new System.Drawing.Point(18, 18);
            this.CboCmd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.CboCmd.MinimumSize = new System.Drawing.Size(63, 0);
            this.CboCmd.Name = "CboCmd";
            this.CboCmd.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.CboCmd.Size = new System.Drawing.Size(589, 35);
            this.CboCmd.Style = Sunny.UI.UIStyle.Custom;
            this.CboCmd.TabIndex = 0;
            this.CboCmd.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.CboCmd.Watermark = "选择执行的命令";
            this.CboCmd.SelectedIndexChanged += new System.EventHandler(this.CboCmd_SelectedIndexChanged);
            // 
            // uiLalPrompt
            // 
            this.uiLalPrompt.BackColor = System.Drawing.Color.White;
            this.uiLalPrompt.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLalPrompt.Location = new System.Drawing.Point(3, 502);
            this.uiLalPrompt.Name = "uiLalPrompt";
            this.uiLalPrompt.Size = new System.Drawing.Size(877, 35);
            this.uiLalPrompt.Style = Sunny.UI.UIStyle.Custom;
            this.uiLalPrompt.TabIndex = 5;
            this.uiLalPrompt.Text = "未扫描";
            this.uiLalPrompt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PortMainFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(883, 542);
            this.Controls.Add(this.uiLalPrompt);
            this.Controls.Add(this.tabMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "PortMainFrm";
            this.Style = Sunny.UI.UIStyle.Custom;
            this.Text = "端口占用助手 v1.2.0";
            this.tabMenu.ResumeLayout(false);
            this.tabPagePortKill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiDgvProcessList)).EndInit();
            this.uiRightMenuProcess.ResumeLayout(false);
            this.tabPageCmdDebug.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UITabControl tabMenu;
        private System.Windows.Forms.TabPage tabPagePortKill;
        private System.Windows.Forms.TabPage tabPageCmdDebug;
        private Sunny.UI.UIStyleManager uiStyleManager1;
        private Sunny.UI.UIButton uiBtnScan;
        private Sunny.UI.UIProcessBar uiProcessBar;
        private Sunny.UI.UIDataGridView uiDgvProcessList;
        private Sunny.UI.UIContextMenuStrip uiRightMenuProcess;
        private System.Windows.Forms.ToolStripMenuItem tsmRefresh;
        private System.Windows.Forms.ToolStripMenuItem tsmKill;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tmsOpenFilePath;
        private System.Windows.Forms.ToolStripMenuItem tmsCopyFilePath;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem tmsFileAttributes;
        private Sunny.UI.UIComboBox CboCmd;
        private Sunny.UI.UITextBox TxtCmd;
        private Sunny.UI.UIButton BtnExecute;
        private Sunny.UI.UIRichTextBox RichTextCmdView;
        private System.Windows.Forms.DataGridViewImageColumn ProcessIcon;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessIdentification;
        private System.Windows.Forms.DataGridViewTextBoxColumn Port;
        private System.Windows.Forms.DataGridViewTextBoxColumn LocalAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn RemoteAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn RemoteIpLocation;
        private System.Windows.Forms.DataGridViewTextBoxColumn State;
        private Sunny.UI.UILabel uiLalPrompt;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem TsmIssues;
        private Sunny.UI.UITextBox uiTxtPort;
    }
}

