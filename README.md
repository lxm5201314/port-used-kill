# PortUsedKill端口占用进程工具

![输入图片说明](img/PortUsedKill.png)

![输入图片说明](img/PortUsedKill2.png)

## PortUsedKill是什么

在做开发时经常因为某种原因项目意外停止，而再启动时却又残留进程没有结束导致端口被占用，因此这个工具就是为了解决这个问题的